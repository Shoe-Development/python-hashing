'''

Author: Shoe
Written: 8/21/2016-TBD
Purpose: Creates password combinations, stores them in SQLite

'''
import os.path
import sqlite3
from Crypto.Hash import MD5, SHA256, SHA512

def commit_close():
    conn.commit()
    conn.close()

def writeToFile(file):
    currentHashCount = 0
    with open(file, 'r', encoding="utf-8") as file:
        for lines in file.readlines():
            lines.encode('utf-8')
            md5hash.update(lines.encode('utf-8'))
            sha256hash.update(lines.encode('utf-8'))
            sha512hash.update(lines.encode('utf-8'))
            print("Finished hashing. Adding to db...")
            insertToDatabase(lines, md5hash, sha256hash, sha512hash)
            currentHashCount += 3

            print("Done successfully! Current hashes: ", currentHashCount)
def insertToDatabase(plaintext, md5hash, sha256hash, sha512hash):
    conn.execute('INSERT INTO Passwords '
                 'VALUES (?, ?, ?, ?)', (plaintext, md5hash.hexdigest(),
                                         sha256hash.hexdigest(),
                                         sha512hash.hexdigest()))
try:
    conn = sqlite3.connect("passwordhouse.db")  # Connect to the DB!

finally:
    print ("Connected to the database.")
md5hash = MD5.new() #Use Pycrypto to enable the hashing of a string => MD5
sha256hash = SHA256.new() #^
sha512hash = SHA512.new() #^^

file = input(
    "Enter the name of the txt file that you want to hash, including "
    "the extension: \n >>>")


if (os.path.isfile(file)):
    writeToFile()
while (not os.path.isfile(file)):
    print ("This file path doesn't exist. Please try again.")
    file = input(
        "Enter the name of the txt file that you want to hash, including "
        "the extension: \n >>>")

writeToFile(file)

print ("Done!")
commit_close()
print ("Closed database.")

